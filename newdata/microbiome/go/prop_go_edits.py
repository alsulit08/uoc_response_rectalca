#!/usr/bin/env python

import pandas as pd
import sys

def main(): 
    godf = pd.read_csv(sys.argv[1], sep="\t", dtype=object, keep_default_na=False).set_index("GO_ID")
    godesc = godf[["Description", "GO"]].copy()
    godesc.to_csv("all_go_propreads_desc.tsv", sep="\t")

    gosams = godf.iloc[:, 2:]
    gosams.to_csv("all_go_propreads_counts.tsv", sep="\t")
    print("Run finished")
    return

if __name__ == "__main__":
    sys.exit(main())
