#!/usr/bin/env python

import pandas as pd
import sys

def main(): 
    c=0
    godf = pd.read_csv(sys.argv[1], sep="\t", dtype=object, keep_default_na=False)
    gosumdf=pd.DataFrame(columns=["GO_ID", "Description", "GO"])
    for gosam in sorted(list(godf["sample"].unique())):
        c+=1
        gosubdf=godf.loc[godf["sample"]==gosam].copy()
        gosubdf=gosubdf[["GO_ID", "Description", "GO", "Proportional_Reads"]]
        gosubdf.rename(columns={"Proportional_Reads":gosam}, inplace=True)
        gosumdf=gosumdf.merge(gosubdf, on=["GO_ID", "Description", "GO"], how="outer")
    
    gosumdf=gosumdf.fillna("0")
    gosumdf=gosumdf.set_index("GO_ID")
    
    #to check if values for each GO_ID is unique:
        #logfile = open(log.sumGOerr, "w")
    checkDesc={}
    checkNS={}
    for i in list(gosumdf.index.values):
        if i not in checkDesc:
            checkDesc[i]=[]
            checkNS[i]=[]
        checkDesc[i].append(gosumdf.at[i, "Description"])
        checkNS[i].append(gosumdf.at[i, "GO"])
    for key1 in checkDesc:
        if len(checkDesc[key1])!=1:
            print("Error: multiple descriptions for {}: {}\n".format(key1, ",".join(checkDesc[key1])))
    for key2 in checkNS:
        if len(checkNS[key2])!=1:
            print("Error: multiple namespaces for {}: {}\n".format(key2, ",".join(checkNS[key2])))
    #            sys.exit(1)
        #logfile.close()
    gosumdf=gosumdf.sort_values(by="Description")
    gosumdf.to_csv("all_go_propreads.tsv", sep="\t")
    print("Run finished")
    return

if __name__ == "__main__":
    sys.exit(main())
