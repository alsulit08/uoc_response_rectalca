---
title: "Rectal_CA_Microbial_Diversity"
author: "ASulit"
#date: "28/05/2021"
output: html_document
knit: (function(inputFile, encoding) { 
          rmarkdown::render(inputFile,
                        encoding=encoding, output_file='RectalCa_MicrobialDiversity.html') })
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
knitr::opts_knit$set(root.dir = "~/Documents/UOC_Work/work/rectal_cancer/bioinformatics/UOC_response_RectalCa/Microbiome/Diversity/")
```

LOAD PACKAGES:

```{r message=FALSE, warning=FALSE, echo=FALSE}
packages <- c("plyr", "tidyverse", "phyloseq", "DESeq2", "microbiome", "vegan", "picante", "ALDEx2", "metagenomeSeq", "HMP", "dendextend", "rms", "data.table", "dplyr", "reshape2", "Rmisc", "ggplot2", "ggpubr", "rstatix", "gplots", "clusterProfiler", "forcats", "stringr", "pheatmap")
for (pkg in packages) {
  library(pkg, character.only = T)
}
print("Loaded packages plyr, tidyverse, phyloseq, DESeq2, microbiome, vegan, picante, ALDEx2, metagenomeSeq, HMP, dendextend, rms, data.table, dplyr, reshape2, Rmisc, ggplot2, ggpubr, rstatix, gplots, clusterProfiler, forcats, stringr, pheatmap")
```

## Metadata and design


1) drop RT3 (S23 and S3)
2) create metadata column that differentiates Complete Responders ("complete") and all other responders ("others"); also include column that will segregate into actual response type


```{r echo=FALSE}
meta <- read.table('../../metadata/rectal_CA_final.txt', sep='\t', quote="", comment.char = "", header=T, row.names=1, check.names = F, na.strings = "", stringsAsFactors = T)
```

```{r echo=FALSE}
## we need to drop s23 and s3 (RT3)
drops <- c("S23", "S3")
meta <- meta[!row.names(meta) %in% drops,]
meta$study_number <- factor(meta$study_number)

meta$cresponse <- rep('others',dim(meta)[1])
meta$cresponse[which(tolower(meta$`Response to RT`) == "no response")] <- "others"
meta$cresponse[which(tolower(meta$`Response to RT`) == "near-complete")] <- "others"
meta$cresponse[which(tolower(meta$`Response to RT`) == "complete response")] <- "complete"
meta$cresponse[which(tolower(meta$`Response to RT`) == "incomplete response")] <- "others"
meta$cresponse <- relevel(as.factor(meta$cresponse), ref = "others")

meta$actualresponse <- rep('',dim(meta)[1])
meta$actualresponse[which(tolower(meta$`Response to RT`) == "no response")] <- "none"
meta$actualresponse[which(tolower(meta$`Response to RT`) == "near-complete")] <- "almost"
meta$actualresponse[which(tolower(meta$`Response to RT`) == "complete response")] <- "complete"
meta$actualresponse[which(tolower(meta$`Response to RT`) == "incomplete response")] <- "incomplete"
meta$actualresponse <- relevel(as.factor(meta$actualresponse), ref = "none")


## order meta row names
meta <- meta[order(row.names(meta)),]
#print(meta)
```

## MICROBIOME 

Strategy:

1. Use bacteria species only
2. I’d focus on the stringent filtered dataset as this was also the dataset we use for differential abundance (>= 10 reads in >= 20% samples)

### Load datasets (microbes), species level

```{r echo=FALSE}
mictbl_filt <- read.table('../../newdata/microbiome/genus/alltaxon_gensp.tsv', sep='\t', quote="", comment.char = "", header=T, row.names=1, check.names = F, stringsAsFactors = T)
#mictbl <- read.table('newdata/microbiome/all_taxon.tsv', sep='\t', quote="", comment.char = "", header=T, row.names=1, check.names = F)
#taxa <- as.matrix(read.table('newdata/microbiome/all_taxonline.tsv', sep='\t', quote="", comment.char = "", header=T, row.names=1, check.names = F))
## this drops RT3 as well
#mictbl <- mictbl[,!colnames(mictbl) %in% drops]
mictbl_filt <-  mictbl_filt[,!colnames(mictbl_filt) %in% drops]
sptbl <- mictbl_filt[mictbl_filt$Species!="",]
## get lineage
sptblline <- sptbl[,1:10]
sptblline <- sptblline[, !colnames(sptblline) %in% c("Taxon", "RootTaxon", "Rank")]
sptbl <- sptbl[,11:dim(sptbl)[2]]
sptblline <- as.matrix(sptblline)
##generate phyloseq
#sptbl <- mictbl
sptbl <- sptbl[,order(colnames(sptbl))]
#mictbl <- mictbl[,order(colnames(mictbl))]

micOTU_filt <- otu_table(sptbl, taxa_are_rows = T)
#micOTU <- otu_table(mictbl, taxa_are_rows = T)
TAX_filt <- tax_table(sptblline)
#TAX <- tax_table(taxa)

cSAM <- sample_data(meta)
micps_filt <- phyloseq(micOTU_filt, TAX_filt, cSAM)
#c_micps <- phyloseq(micOTU, TAX, cSAM)

print("Phyloseq with all species (no filtering)")
micps_filt
#print("Phyloseq with all species (Metafunc filtering [0.01% in 1 sample])")
#c_micps

all4filt.sums <- data.frame(sample_sums(micps_filt))
names(all4filt.sums) <- "all_species"
#all.sums <- data.frame(sample_sums(c_micps))
#names(all.sums) <- "all_species"

##use only Bacteria
micps_filt <- subset_taxa(micps_filt, Kingdom=="Bacteria")
#c_micps <- subset_taxa(c_micps, Kingdom=="Bacteria")
print("Phyloseq with only Bacterial species (all species):")
micps_filt
#print("Phyloseq with only Bacterial species (Metafunc filtered):")
#c_micps
bact4filt.sums <- data.frame(sample_sums(micps_filt))
names(bact4filt.sums) <- "all_Bacteria_species"
#bact.sums <- data.frame(sample_sums(c_micps))
#names(bact.sums) <- "all_Bacteria_species"

##use phyloseq to filter Bacteria 10 reads in 20% of samples <only for the non-metafunc filtered>
micps_filt <- filter_taxa(micps_filt, function(x) sum(x >= 10) >= (0.2*length(x)), TRUE)
print("Phyloseq after filtering (10 reads in 20% samples). For stricter filters:")
micps_filt
filt.sums <- data.frame(sample_sums(micps_filt))
names(filt.sums) <- "filtered_Bacteria_species"
combinedsums4filt <- merge(all4filt.sums, bact4filt.sums, by=0)
row.names(combinedsums4filt) <- combinedsums4filt$Row.names
combinedsums4filt$Row.names <- NULL
combinedsums4filt <- merge(combinedsums4filt, filt.sums, by=0)
row.names(combinedsums4filt) <- combinedsums4filt$Row.names
combinedsums4filt$Row.names <- NULL
print("Sample sizes, species level for stringent filters:")
print(combinedsums4filt)

#combinedsums <- merge(all.sums, bact.sums, by=0)
#row.names(combinedsums) <- combinedsums$Row.names
#combinedsums$Row.names <- NULL
#print("Sample sizes, species level for metafunc filters:")
#print(combinedsums)
```

```{r eval=FALSE, include=FALSE}
### Load reformated metadata with the species table into a phyloseq object
taxa_names(micps_filt) <- tax_table(micps_filt)[,"Species"]
```


### Histogram of sample sizes after filtering

```{r echo=FALSE}
print("Histogram for stringent filters:")
samsums <- data.frame(sample_sums(micps_filt))
#samsums[order(samsums$sample_sums.micps_filt.), , drop=F]
#print("sorted sample sizes after filtering:")
print(sort(samsums$sample_sums.micps_filt.))
#print("Minimum sample size:")
#sort(samsums$sample_sums.micps_filt.)[1]
#print("Maximum sample size:")
#tail(sort(samsums$sample_sums.micps_filt.), n=1)
print("Distribution of sample sizes:")
qplot(samsums$sample_sums.micps_filt., geom="histogram", bins=50) + xlab("read counts matching to species: all samples")
print("The following are the 3 samples with the largest sample sizes:")
print("S55:	2777574")
print("S8:	4787394")
print("S4:	5845561")

#print("Histogram for Metafunc filters:")
#samsums <- data.frame(sample_sums(c_micps))
#samsums[order(samsums$sample_sums.c_micps.), , drop=F]
#print("sorted sample sizes after filtering:")
#print(sort(samsums$sample_sums.c_micps.))
#print("Minimum sample size:")
#sort(samsums$sample_sums.c_micps.)[1]
#print("Maximum sample size:")
#tail(sort(samsums$sample_sums.c_micps.), n=1)
#print("Distribution of sample sizes:")
#qplot(samsums$sample_sums.c_micps., geom="histogram", bins=50) + xlab("read counts matching to species: all samples")
#print("The following are the 3 samples with the largest sample sizes:")
#print("S8:	2783101")
#print("S80:	4789339")
#print("S9:	5864196")
```

### Alpha Diversity

```{r}
psfilt_rare <-rarefy_even_depth(micps_filt, rngseed = 1,
                             sample.size = 0.9*min(sample_sums(micps_filt)),
                             replace = FALSE)


print("rarefied stringent filters:")
psfilt_rare

print("Library sizes before and after filtering:")
rareinfo_filt <- data.frame(dataset = "stringent filters", og_minSize = min(sample_sums(otu_table(micps_filt))), og_maxSize = max(sample_sums(otu_table(micps_filt))), rareSize = min(sample_sums(psfilt_rare)))
print(rareinfo_filt)
```

```{r}
print("Normality tests for alpha diversity:")

alpha_summary_filt <- estimate_richness(psfilt_rare, measures = c("Observed", "Shannon"))

afiltshap <- data.frame(dataset = "stringent filters", shap_obs_p = shapiro.test(alpha_summary_filt$Observed)$p.value, shap_shan_p = shapiro.test(alpha_summary_filt$Shannon)$p.value)
print(afiltshap)

#ggarrange(hist(alpha_summary_filt$Observed), hist(alpha_summary$Observed))

hist(alpha_summary_filt$Observed, main="Observed Values for Stringent Filters", xlab = "Observed")

hist(alpha_summary_filt$Shannon, main="Shannon Values for Stringent Filters", xlab = "Shannon")
```

- Test for differences. Use Wilcoxon as one fails normality tests.

```{r}
alphafiltwmeta <- merge(alpha_summary_filt, meta, by = 0, all=T)
#alphawmeta <- merge(alpha_summary, meta, by=0, all=T)

orderedfilt <- alphafiltwmeta[order(alphafiltwmeta$study_number),]
orderedfilt <- orderedfilt[order(orderedfilt$tissue_type),]
#ordered <- alphawmeta[order(alphawmeta$study_number),]
#ordered <- ordered[order(ordered$tissue_type),]

print("Test for differences overall, stringent filters. Compare Tumor vs Normal (paired test), and Complete vs Other responders (Unpaired test).:")

print(rbind(
data.frame(wilcox_test(orderedfilt, Observed ~ tissue_type, ref.group="normal", p.adjust.method = "BH", paired=T)),
data.frame(wilcox_test(orderedfilt, Observed ~ cresponse, ref.group="others", p.adjust.method = "BH"))
))
print(rbind(
data.frame(wilcox_test(orderedfilt, Shannon ~ tissue_type, ref.group="normal", p.adjust.method = "BH", paired=T)),
data.frame(wilcox_test(orderedfilt, Shannon ~ cresponse, ref.group="others", p.adjust.method = "BH"))
))

#print("Test for differences overall, metafunc filters. Compare Tumor vs Normal (paired test), and Complete vs Other responders (Unpaired test).:")
#print(rbind(
#data.frame(wilcox_test(ordered, Observed ~ tissue_type, ref.group="normal", p.adjust.method = "BH", paired=T)),
#data.frame(wilcox_test(ordered, Observed ~ cresponse, ref.group="others", p.adjust.method = "BH"))
#))
#print(rbind(
#data.frame(wilcox_test(ordered, Shannon ~ tissue_type, ref.group="normal", p.adjust.method = "BH", paired=T)),
#data.frame(wilcox_test(ordered, Shannon ~ cresponse, ref.group="others", p.adjust.method = "BH"))
#))

print("Test for differences grouped by either tissue type or response, stringent filters. Compare Tumor vs Normal (paired test), and Complete vs Other responders (Unpaired test).:")

orderedfilt %>% group_by(tissue_type) %>% wilcox_test(Observed ~ cresponse, ref.group = "others", p.adjust.method = "BH")
orderedfilt %>% group_by(tissue_type) %>% wilcox_test(Shannon ~ cresponse, ref.group = "others", p.adjust.method = "BH")
orderedfilt %>% group_by(cresponse) %>% wilcox_test(Observed ~ tissue_type, ref.group="normal", p.adjust.method = "BH", paired=T)
orderedfilt %>% group_by(cresponse) %>% wilcox_test(Shannon ~ tissue_type, ref.group="normal", p.adjust.method = "BH", paired=T)


#print("Test for differences grouped by either tissue type or response, metafunc filters. Compare Tumor vs Normal (paired test), and Complete vs Other responders (Unpaired test).:")

#ordered %>% group_by(tissue_type) %>% wilcox_test(Observed ~ cresponse)
#ordered %>% group_by(tissue_type) %>% wilcox_test(Shannon ~ cresponse)
#ordered %>% group_by(cresponse) %>% wilcox_test(Observed ~ tissue_type, ref.group="normal", p.adjust.method = "BH", paired=T)
#ordered %>% group_by(cresponse) %>% wilcox_test(Shannon ~ tissue_type, ref.group="normal", p.adjust.method = "BH", paired=T)
```

- Alpha Diversity Plots

```{r fig.height=4, fig.width=4}

print("Observed alpha diversity:")
ggplot(orderedfilt, aes(x=tissue_type,y=Observed)) + geom_boxplot() + theme(axis.text.x = element_text(angle = 45)) + geom_point() + stat_compare_means(method="wilcox", paired=T) + geom_line(aes(group=study_number, color=study_number), linetype="dashed") + theme(legend.position = "none") + ggtitle("stringent filters")
#, ggplot(ordered, aes(x=tissue_type,y=Observed)) + geom_boxplot() + theme(axis.text.x = element_text(angle = 45)) + geom_point() + stat_compare_means(method="wilcox", paired=T) + geom_line(aes(group=study_number, color=study_number), linetype="dashed") + theme(legend.position = "none") + ggtitle("metafunc filters")

ggplot(orderedfilt, aes(x=cresponse,y=Observed)) + geom_boxplot() + theme(axis.text.x = element_text(angle = 45)) + geom_point() + stat_compare_means(method="wilcox") + ggtitle("stringent filters")
#, ggplot(ordered, aes(x=cresponse,y=Observed)) + geom_boxplot() + theme(axis.text.x = element_text(angle = 45)) + geom_point() + stat_compare_means(method="wilcox") + ggtitle("metafunc filters")

ggplot(orderedfilt, aes(x=cresponse, y=Observed)) + geom_boxplot() + facet_wrap(~tissue_type) + geom_point() + stat_compare_means(method="wilcox") + ggtitle("stringent filters")
#, ggplot(ordered, aes(x=cresponse, y=Observed)) + geom_boxplot() + facet_wrap(~tissue_type) + geom_point() + stat_compare_means(method="wilcox") + ggtitle("metafunc filters"))

ggplot(orderedfilt, aes(x=tissue_type, y=Observed)) + geom_boxplot() + facet_wrap(~cresponse) + geom_point() + stat_compare_means(method="wilcox", paired=T) + geom_line(aes(group=study_number, color=study_number), linetype="dashed") + theme(legend.position = "none") + ggtitle("stringent filters")
#, ggplot(ordered, aes(x=tissue_type, y=Observed)) + geom_boxplot() + facet_wrap(~cresponse) + geom_point() + stat_compare_means(method="wilcox", paired=T) + geom_line(aes(group=study_number, color=study_number), linetype="dashed") + theme(legend.position = "none") + ggtitle("metafunc filters"))


print("Shannon alpha diversity:")

ggplot(orderedfilt, aes(x=tissue_type,y=Shannon)) + geom_boxplot() + theme(axis.text.x = element_text(angle = 45)) + geom_point() + stat_compare_means(method="wilcox", paired=T) + geom_line(aes(group=study_number, color=study_number), linetype="dashed") + theme(legend.position = "none") + ggtitle("stringent filters")
#, ggplot(ordered, aes(x=tissue_type,y=Shannon)) + geom_boxplot() + theme(axis.text.x = element_text(angle = 45)) + geom_point() + stat_compare_means(method="wilcox", paired=T) + geom_line(aes(group=study_number, color=study_number), linetype="dashed") + theme(legend.position = "none") + ggtitle("metafunc filters")

ggplot(orderedfilt, aes(x=cresponse,y=Shannon)) + geom_boxplot() + theme(axis.text.x = element_text(angle = 45)) + geom_point() + stat_compare_means(method="wilcox") + ggtitle("stringent filters")
#, ggplot(ordered, aes(x=cresponse,y=Shannon)) + geom_boxplot() + theme(axis.text.x = element_text(angle = 45)) + geom_point() + stat_compare_means(method="wilcox") + ggtitle("metafunc filters")

ggplot(orderedfilt, aes(x=cresponse, y=Shannon)) + geom_boxplot() + facet_wrap(~tissue_type) + geom_point() + stat_compare_means(method="wilcox") + ggtitle("stringent filters")
#, ggplot(ordered, aes(x=cresponse, y=Shannon)) + geom_boxplot() + facet_wrap(~tissue_type) + geom_point() + stat_compare_means(method="wilcox") + ggtitle("metafunc filters"))

ggplot(orderedfilt, aes(x=tissue_type, y=Shannon)) + geom_boxplot() + facet_wrap(~cresponse) + geom_point() + stat_compare_means(method="wilcox", paired=T) + geom_line(aes(group=study_number, color=study_number), linetype="dashed") + theme(legend.position = "none") + ggtitle("stringent filters")
#, ggplot(ordered, aes(x=tissue_type, y=Shannon)) + geom_boxplot() + facet_wrap(~cresponse) + geom_point() + stat_compare_means(method="wilcox", paired=T) + geom_line(aes(group=study_number, color=study_number), linetype="dashed") + theme(legend.position = "none") + ggtitle("metafunc filters"))
```


### Beta Diversity

-  Beta Diversity refers to community differences between groups, I focused on the stringent filtered dataset as this was also the dataset we use for differential abundance

```{r}
print("Use rarefied dataset, bray curtis distance, NMDS ordination")
sample_data(psfilt_rare)$cresponse <- factor(sample_data(psfilt_rare)$cresponse,levels = c("others", "complete"))
sample_data(psfilt_rare)$tissue_type <- factor(sample_data(psfilt_rare)$tissue_type,levels = c("normal", "tumor"))
set.seed(1000)
ord.nmds.bray <- ordinate(psfilt_rare, "NMDS", "bray", trymax=200)
```
```{r}
ord.nmds.bray
```
***stress is 0.1 and is considered 'good'***

```{r}
stressplot(ord.nmds.bray)
```

```{r}
p1 <- plot_ordination(psfilt_rare, ord.nmds.bray, type="samples", color="tissue_type")
p2 <- p1 + stat_ellipse(aes(group = tissue_type), linetype=2)
print("NMDS: Tumor vs Normal")
p2
p1.1 <- plot_ordination(psfilt_rare, ord.nmds.bray, type="samples", color="cresponse")
print("NMDS: Response")
p1.1 + stat_ellipse(aes(group = cresponse), linetype=2)

p3 <- plot_ordination(psfilt_rare, ord.nmds.bray, type="samples", color="cresponse") + facet_wrap(~tissue_type) + stat_ellipse(aes(group = cresponse, color=cresponse), linetype = 2)
print("NMDS: response by tissue type")
p3
p4 <- plot_ordination(psfilt_rare, ord.nmds.bray, type="samples", color="tissue_type") + facet_wrap(~cresponse) + stat_ellipse(aes(group = tissue_type, color=tissue_type), linetype = 2)
print("NMDS: tissue type by response")
p4
#ggarrange(p2, p3, nrow=2, ncol=1)
```

***Above facets divides the samples by the variable but the entire ordination or clustering is still affected by all samples, below we subset samples into groups accordingly***

```{r}
psrare_cresp <- subset_samples(psfilt_rare, cresponse == 
                                 "complete")
psrare_cresp <- phyloseq::prune_taxa(phyloseq::taxa_sums(psrare_cresp) > 0, psrare_cresp)
psrare_cresp

psrare_oresp <- subset_samples(psfilt_rare, cresponse ==  "others")
psrare_oresp <- phyloseq::prune_taxa(phyloseq::taxa_sums(psrare_oresp) > 0, psrare_oresp)
psrare_oresp

psrare_tumor <- subset_samples(psfilt_rare, tissue_type ==  "tumor")
psrare_tumor <- phyloseq::prune_taxa(phyloseq::taxa_sums(psrare_tumor) > 0, psrare_tumor)
psrare_tumor

psrare_norm <- subset_samples(psfilt_rare, tissue_type ==  "normal")
psrare_norm <- phyloseq::prune_taxa(phyloseq::taxa_sums(psrare_norm) > 0, psrare_norm)
psrare_norm

set.seed(1000)
ord.nmds.cresp <- ordinate(psrare_cresp, "NMDS", "bray", trymax=30)
set.seed(1000)
ord.nmds.oresp <- ordinate(psrare_oresp, "NMDS", "bray", trymax=30)
set.seed(1000)
ord.nmds.tum <- ordinate(psrare_tumor, "NMDS", "bray", trymax=30)
set.seed(1000)
ord.nmds.norm <- ordinate(psrare_norm, "NMDS", "bray", trymax=30)

ord.nmds.cresp
stressplot(ord.nmds.cresp)
ord.nmds.oresp
stressplot(ord.nmds.oresp)
ord.nmds.tum
stressplot(ord.nmds.tum)
ord.nmds.norm
stressplot(ord.nmds.norm)
```
```{r}
print("NMDS: T v N in complete responders")
plot_ordination(psrare_cresp, ord.nmds.cresp, type="samples", color="tissue_type") + stat_ellipse(aes(group = tissue_type), linetype=2)
print("NMDS: T v N in other responders")
plot_ordination(psrare_oresp, ord.nmds.oresp, type="samples", color="tissue_type") + stat_ellipse(aes(group = tissue_type), linetype=2)
print("NMDS: response in tumors")
plot_ordination(psrare_tumor, ord.nmds.tum, type="samples", color="cresponse") + stat_ellipse(aes(group = cresponse), linetype=2)
print("NMDS: response in normal samples")
plot_ordination(psrare_norm, ord.nmds.norm, type="samples", color="cresponse") + stat_ellipse(aes(group = cresponse), linetype=2)
```

#### Beta Diversity: non-rarefied

- use centered log-ratios, ordinate using RDA (but unconstrained ~ PCA) with euclidean distance (since CLR == aitchison distance)

```{r}
ps_clr <- microbiome::transform(micps_filt, "clr")
#set.seed(1000)
#ord.nmds.clr <- ordinate(ps_clr, "NMDS", "euclidean", trymax=100)
ord.euc.clr <- ordinate(ps_clr, "RDA", "euclidean")
#ord.nmds.clr
#stressplot(ord.nmds.bray)
```

```{r}
plot_ordination(ps_clr, ord.euc.clr, type="samples", color="tissue_type") + stat_ellipse(aes(group = tissue_type), linetype=2)
plot_ordination(ps_clr, ord.euc.clr, type="samples", color="cresponse") + stat_ellipse(aes(group = cresponse), linetype=2)
plot_ordination(ps_clr, ord.euc.clr, type="samples", color="cresponse") + facet_wrap(~tissue_type) + stat_ellipse(aes(group = cresponse, color=cresponse), linetype = 2)
plot_ordination(ps_clr, ord.euc.clr, type="samples", color="tissue_type") + facet_wrap(~cresponse) + stat_ellipse(aes(group = tissue_type, color=tissue_type), linetype = 2)

#ggarrange(p2, p3, nrow=2, ncol=1)
```

***as above, facet wraps divide the samples accordingly but ordination is still based on overall samples***

```{r}
clr_cresp <- subset_samples(micps_filt, cresponse == 
                                 "complete")
clr_cresp <- phyloseq::prune_taxa(phyloseq::taxa_sums(clr_cresp) > 0, clr_cresp)
clr_cresp <- microbiome::transform(clr_cresp, "clr")
clr_cresp

clr_oresp <- subset_samples(micps_filt, cresponse ==  "others")
clr_oresp <- phyloseq::prune_taxa(phyloseq::taxa_sums(clr_oresp) > 0, clr_oresp)
clr_oresp <-  microbiome::transform(clr_oresp, "clr")
clr_oresp

clr_tumor <- subset_samples(micps_filt, tissue_type ==  "tumor")
clr_tumor <- phyloseq::prune_taxa(phyloseq::taxa_sums(clr_tumor) > 0, clr_tumor)
clr_tumor <-  microbiome::transform(clr_tumor, "clr")
clr_tumor


clr_norm <- subset_samples(micps_filt, tissue_type ==  "normal")
clr_norm <- phyloseq::prune_taxa(phyloseq::taxa_sums(clr_norm) > 0, clr_norm)
clr_norm <-  microbiome::transform(clr_norm, "clr")
clr_norm

set.seed(1000)
ord.rda.cresp <- ordinate(clr_cresp, "RDA", "euclidean")
set.seed(1000)
ord.rda.oresp <- ordinate(clr_oresp, "RDA", "euclidean")
set.seed(1000)
ord.rda.tum <- ordinate(clr_tumor, "RDA", "euclidean")
set.seed(1000)
ord.rda.norm <- ordinate(clr_norm, "RDA", "euclidean")
```

```{r}
plot_ordination(clr_cresp, ord.rda.cresp, type="samples", color="tissue_type") + stat_ellipse(aes(group = tissue_type), linetype=2)

plot_ordination(clr_oresp, ord.rda.oresp, type="samples", color="tissue_type") + stat_ellipse(aes(group = tissue_type), linetype=2)

plot_ordination(clr_tumor, ord.rda.tum, type="samples", color="cresponse") + stat_ellipse(aes(group = cresponse), linetype=2)

plot_ordination(clr_norm, ord.rda.norm, type="samples", color="cresponse") + stat_ellipse(aes(group = cresponse), linetype=2)
```


### Plots

```{r}
##use stringent filters for alpha diversity
obs.tis <- ggplot(orderedfilt, aes(x=tissue_type,y=Observed)) + geom_boxplot() + theme(axis.text.x = element_text(angle = 45)) + geom_point() + stat_compare_means(method="wilcox", paired=T) + geom_line(aes(group=study_number, color=study_number), linetype="dashed") + theme(legend.position = "none") + xlab("tissue type") + ylab("")

shan.tis <- ggplot(orderedfilt, aes(x=tissue_type,y=Shannon)) + geom_boxplot() + theme(axis.text.x = element_text(angle = 45)) + geom_point() + stat_compare_means(method="wilcox", paired=T) + geom_line(aes(group=study_number, color=study_number), linetype="dashed") + theme(legend.position = "none") + xlab("tissue type") + ylab("")

obs.res <- ggplot(orderedfilt, aes(x=cresponse,y=Observed)) + geom_boxplot() + theme(axis.text.x = element_text(angle = 45)) + geom_point() + stat_compare_means(method="wilcox") + xlab("response") + ylab("")

shan.res <- ggplot(orderedfilt, aes(x=cresponse,y=Shannon)) + geom_boxplot() + theme(axis.text.x = element_text(angle = 45)) + geom_point() + stat_compare_means(method="wilcox") + xlab("response") + ylab("")

obs.resbytis <- ggplot(orderedfilt, aes(x=cresponse, y=Observed)) + geom_boxplot() + facet_wrap(~tissue_type) + geom_point() + stat_compare_means(method="wilcox") + xlab("response") + ylab("")

shan.resbytis <- ggplot(orderedfilt, aes(x=cresponse, y=Shannon)) + geom_boxplot() + facet_wrap(~tissue_type) + geom_point() + stat_compare_means(method="wilcox") + xlab("response") + ylab("")

obs.tisbyres <- ggplot(orderedfilt, aes(x=tissue_type, y=Observed)) + geom_boxplot() + facet_wrap(~cresponse) + geom_point() + stat_compare_means(method="wilcox", paired=T) + geom_line(aes(group=study_number, color=study_number), linetype="dashed") + theme(legend.position = "none") + xlab("tissue type") + ylab("")

shan.tisbyres <- ggplot(orderedfilt, aes(x=tissue_type, y=Shannon)) + geom_boxplot() + facet_wrap(~cresponse) + geom_point() + stat_compare_means(method="wilcox", paired=T) + geom_line(aes(group=study_number, color=study_number), linetype="dashed") + theme(legend.position = "none") + xlab("tissue type") + ylab("")
```

```{r}
##beta diversity, NMDS
nmds.tis <- plot_ordination(psfilt_rare, ord.nmds.bray, type="samples", color="tissue_type") + stat_ellipse(aes(group = tissue_type), linetype=2) + scale_color_manual(values=c("deepskyblue3", "darksalmon")) + guides(color=guide_legend(title="tissue type"))

nmds.res <- plot_ordination(psfilt_rare, ord.nmds.bray, type="samples", color="cresponse") + stat_ellipse(aes(group = cresponse), linetype=2) + scale_color_manual(values=c("purple", "darkorange")) + guides(color=guide_legend(title="response"))

nmds.resbytis <- plot_ordination(psfilt_rare, ord.nmds.bray, type="samples", color="cresponse") + facet_wrap(~tissue_type) + stat_ellipse(aes(group = cresponse, color=cresponse), linetype = 2) + scale_color_manual(values=c("purple", "darkorange")) + guides(color=guide_legend(title="response"))

nmds.tisbyres <- plot_ordination(psfilt_rare, ord.nmds.bray, type="samples", color="tissue_type") + facet_wrap(~cresponse) + stat_ellipse(aes(group = tissue_type, color=tissue_type), linetype = 2) + scale_color_manual(values=c("deepskyblue3", "darksalmon")) + guides(color=guide_legend(title="tissue type"))
```

```{r fig.height=10, fig.width=16}
div1 <- annotate_figure(ggarrange(obs.tis,
          obs.res,
          obs.resbytis,
          obs.tisbyres, labels = c("A", "B", "C", "D"), nrow=1), left = text_grob("Observed", rot = 90, size=18))
div2 <- annotate_figure(ggarrange(shan.tis,
          shan.res,
          shan.resbytis,
          shan.tisbyres, labels = c( "E", "F", "G", "H"), nrow=1), left = text_grob("Shannon", rot = 90, size=18))
div3 <- ggarrange(nmds.tis,
          nmds.res,
          nmds.resbytis,
          nmds.tisbyres, 
          labels=c("I", "J", "K", "L"), nrow=1)
div <- ggarrange(div1, div2, div3, nrow=3)
div
```

```{r}
sessionInfo()
```