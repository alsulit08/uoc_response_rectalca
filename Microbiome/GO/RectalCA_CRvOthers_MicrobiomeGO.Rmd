---
title: "go_plots"
author: "ASulit"
#date: "08/07/2021"
output: html_document
knit: (function(inputFile, encoding) { 
          rmarkdown::render(inputFile,
          envir = globalenv(),
          encoding=encoding,
          output_file='RectalCA_CRvOthers_MicGO') })
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
#knitr::opts_knit$set(root.dir = "~/Documents/UOC_Work/work/rectal_cancer/bioinformatics/UOC_response_RectalCa/Microbiome/GO/")
```

```{r message=FALSE, warning=FALSE, echo=FALSE}
packages <- c("plyr", "tidyverse", "HMP", "dendextend", "rms", "data.table", "dplyr", "reshape2", "Rmisc", "ggplot2", "ggpubr", "rstatix", "gplots", "stringr", "pheatmap", "viridis", "corrplot")
for (pkg in packages) {
  library(pkg, character.only = T)
}
print("Loaded packages plyr, tidyverse, phyloseq, DESeq2, microbiome, vegan, picante, ALDEx2, metagenomeSeq, HMP, dendextend, rms, data.table, dplyr, reshape2, Rmisc, ggplot2, ggpubr, rstatix, gplots, clusterProfiler, forcats, stringr, pheatmap")
```



## Metadata

```{r echo=FALSE}
meta <- read.table('../../metadata/rectal_CA_final.txt', sep='\t', quote="", comment.char = "", header=T, row.names=1, check.names = F, na.strings = "", stringsAsFactors = T)
drops <- c("S23", "S3")
meta <- meta[!row.names(meta) %in% drops,]
meta$study_number <- factor(meta$study_number)

meta$cresponse <- rep('others',dim(meta)[1])
meta$cresponse[which(tolower(meta$`Response to RT`) == "no response")] <- "others"
meta$cresponse[which(tolower(meta$`Response to RT`) == "near-complete")] <- "others"
meta$cresponse[which(tolower(meta$`Response to RT`) == "complete response")] <- "complete"
meta$cresponse[which(tolower(meta$`Response to RT`) == "incomplete response")] <- "others"
meta$cresponse <- relevel(as.factor(meta$cresponse), ref = "others")

meta <- meta[order(row.names(meta)),]
meta$tissue_response <- rep('',dim(meta)[1])
meta[(meta$tissue_type=="tumor") & (meta$cresponse)=='complete', "tissue_response"] <- "t_complete"
meta[(meta$tissue_type=="tumor") & (meta$cresponse)=='others', "tissue_response"] <- "t_others"
meta[(meta$tissue_type=="normal") & (meta$cresponse)=='others', "tissue_response"] <- "n_others"
meta[(meta$tissue_type=="normal") & (meta$cresponse)=='complete', "tissue_response"] <- "n_complete"

#meta <- meta[,c("tissue_response"), drop=F]
#meta <- meta[order(meta$tissue_response),,drop=F]

meta$PatientTissue <- paste0(meta$study_number,"_", meta$tissue_type)
pairidx <- with(meta,order(PatientTissue))
```

## GO plots

```{r}
df <- read.table("../../newdata/microbiome/go/newgo/DAgroup_go/samples_Bacteria_prop.gofile.tsv", sep="\t", comment.char = "", quote = "", header = T, stringsAsFactors = F)
df <- df[!df$sample %in% drops,]

forpropdf <- df[,c("Description", "Proportional_Reads", "sample")]
##get raw counts
propdf <- data.frame()
for (s in unique(forpropdf$sample)) {
  dfsam <- forpropdf[forpropdf$sample==s,]
  row.names(dfsam) <- dfsam$Description
  dfsam$Description <- NULL
  dfsam <- dfsam[,!colnames(dfsam) %in% c("sample"), drop=F]
  names(dfsam) <- s
  #names(dfbpsam) <- paste0(paste0(l, "_"), name)
  propdf <- merge(propdf, dfsam, by.y = 0, by.x=0, all = T)
  row.names(propdf) <- propdf$Row.names
  propdf$Row.names <- NULL
  }
propdf[is.na(propdf)] <- 0
## order bactBP colnames to match meta
propdf <- propdf[, order(colnames(propdf))]
propdf[is.na(propdf)] <- 0
propfilt <- propdf[rowSums(propdf >= 10) >= 7.8,]

dfbp <- df[df$GO=="BP",]
## subset for wanted columns
dfbp <- dfbp[,c("Description", "sp_Percent", "sample")]

bactBP <- data.frame()
for (l in unique(dfbp$sample)) {
  dfbpsam <- dfbp[dfbp$sample==l,]
  row.names(dfbpsam) <- dfbpsam$Description
  dfbpsam$Description <- NULL
  dfbpsam <- dfbpsam[,!colnames(dfbpsam) %in% c("sample"), drop=F]
  names(dfbpsam) <- l
  #names(dfbpsam) <- paste0(paste0(l, "_"), name)
  bactBP <- merge(bactBP, dfbpsam, by.y = 0, by.x=0, all = T)
  row.names(bactBP) <- bactBP$Row.names
  bactBP$Row.names <- NULL
  }
bactBP[is.na(bactBP)] <- 0
## order bactBP colnames to match meta
bactBP <- bactBP[, order(colnames(bactBP))]
## rename columns to RT_and tissue
colnames(bactBP) <- meta$PatientTissue
## reorder bactBP to pair id order
bactBP <- bactBP[,pairidx]
## add 0.1 to all because we will divide
bactBP <- bactBP + 0.1
FCbp <- matrix(0,dim(bactBP)[1],39)
abp <- c()
for (n in 1:39){
  FCbp[,n] <- bactBP[,2*n]/bactBP[,2*n-1]
  abp <- c(abp, strsplit(colnames(bactBP)[2*n],'_')[[1]][1])
  }
colnames(FCbp) <- abp
rownames(FCbp) <- rownames(bactBP)

     
## same as above but for MF GO
dfmf <- df[df$GO=="MF",]
dfmf <- dfmf[,c("Description", "sp_Percent", "sample")]

bactMF <- data.frame()
for (m in unique(dfmf$sample)){
  dfmfsam <- dfmf[dfmf$sample==m,]
  row.names(dfmfsam) <- dfmfsam$Description
  dfmfsam$Description <- NULL
  dfmfsam <- dfmfsam[,!colnames(dfmfsam) %in% c("sample"), drop=F]
  names(dfmfsam) <- m
  #names(dfmfsam) <- paste0(paste0(m, "_"), name)
  bactMF <- merge(bactMF, dfmfsam, by.y = 0, by.x=0, all = T)
  row.names(bactMF) <- bactMF$Row.names
  bactMF$Row.names <- NULL
  }

bactMF[is.na(bactMF)] <- 0
## order  colnames to match meta
bactMF <- bactMF[, order(colnames(bactMF))]
## rename columns to RT_and tissue
colnames(bactMF) <- meta$PatientTissue
## reorder  to pair id order
bactMF <- bactMF[,pairidx]
bactMF <- bactMF + 0.1
FCmf <- matrix(0,dim(bactMF)[1],39)
amf <- c()
for (n2 in 1:39){
  FCmf[,n2] <- bactMF[,2*n2]/bactMF[,2*n2-1]
  amf <- c(amf, strsplit(colnames(bactMF)[2*n2],'_')[[1]][1])
  }
colnames(FCmf) <- amf
rownames(FCmf) <- rownames(bactMF)

## rename bactBP and bactMF for easier coding <based on old code>
alldfbp <- data.frame(FCbp, check.names=F)
alldfmf <- data.frame(FCmf, check.names=F)
```

```{r}
## get godepths and also check if consistent depths throughout files
# no reason it should be inconsistent as this is from goatools; depth is the maximum distance from top while level is minimum distance from top

#df <- read.table(paste0("newdata/microbiome/go/newgo/", paste0(f, "/samples_Bacteria_prop.gofile.tsv")), sep="\t", comment.char = "", quote = "", header = T, stringsAsFactors = F)
godf <- df[(df$Description %in% row.names(alldfbp)) | (df$Description %in% row.names(alldfmf)),]

## there has to be a faster way (and already checked for duplicates):
#godepths <- list()
#for (r in row.names(godf)){
#  goname = godf[r, "Description"]
#  dep = as.character(godf[r, "GO_Depth"])
#  if (goname %in% names(godepths)){
#    if (as.character(godepths[goname]) != dep){
#      print(paste0("different depths:", goname))
#      }
#    } else {
#      godepths[[goname]] <- c()
#      godepths[[goname]] <- c(godepths[[goname]], as.character(godf[r, "GO_Depth"]))
#     }
#    } 
#
##convert depths to df for easier access
#godepthdf <- as.data.frame(matrix(0, nrow = 1))
#for (n in names(godepths)){
#  newgodf <- data.frame(n = godepths[n], check.names=F)
#  godepthdf <- cbind(godepthdf, newgodf)
#}
#godepthdf <- godepthdf[,-1]
#godepthdf <- data.frame(t(godepthdf), check.names = F)
godepthdf <- godf[,c("Description", "GO_Depth")]
godepthdf <- unique(godepthdf)
row.names(godepthdf) <- godepthdf$Description
godepthdf$Description <- NULL
names(godepthdf) <- "depth"
godepthdf$depth <- as.numeric(as.character(godepthdf$depth))
#alldfbp2
dim(godepthdf)
#rm(godepths)
```

- reorder alldfbp and alldfmf

```{r}

length(union(colnames(alldfbp), colnames(alldfmf)))
setdiff(colnames(alldfbp), colnames(alldfmf))
setdiff(colnames(alldfmf), colnames(alldfbp))
## create data frame with the proper groups (can just use alldfbp2)
#resp <- as.data.frame(matrix(0, nrow = 1))
#for (c in colnames(alldfbp)){
#  response <- as.character(unique(meta[meta$study_number==c, "cresponse"]))
  #tempdf <- data.frame(c, response)
  #tempdf <- data.frame(t(tempdf))
  #names(tempdf) <- tempdf[1,]
  #tempdf <- tempdf[-1,,drop=F]
  #resp <- cbind(resp, tempdf)
#}
resp <- meta[meta$tissue_type=="tumor",c("study_number", "cresponse")]
row.names(resp) <- resp$study_number
resp <- resp[,c("cresponse"), drop=F]
names(resp) <- "response"

resp2 <- resp
neword <- c()
for (t in c("complete", "others")){
  cnames <- row.names(resp2[resp2$response==t,,drop=F])
  for (c in cnames){
    neword <- c(neword, c)
  }
}

alldfbp <- alldfbp[,neword]
alldfmf <- alldfmf[,neword]

alldfbpfilt <- alldfbp[row.names(alldfbp) %in% row.names(propfilt),]
alldfmffilt <- alldfmf[row.names(alldfmf) %in% row.names(propfilt),]
##heatmap classification colors
classcolor <- list(response=c(complete = "red", others = "purple"))

#aClassification <- aClassification[order(aClassification$study_number),]
#row.names(aClassification) <- aClassification$study_number
#aClassification <- aClassification[, c("actualresponse"), drop=F]
```

```{r eval=FALSE, fig.height=30, fig.width=40, include=FALSE}
#breaksList = seq(0, 10, by = 1)
lowbpdf <- alldfbp[row.names(alldfbp) %in% row.names(godepthdf[godepthdf$depth < 5 ,,drop=F]),]
midlowbpdf <- alldfbp[row.names(alldfbp) %in% row.names(godepthdf[(godepthdf$depth > 4) & (godepthdf < 11) ,,drop=F]),]
midhibpdf <- alldfbp[row.names(alldfbp) %in% row.names(godepthdf[(godepthdf$depth > 10) & (godepthdf < 13) ,,drop=F]),]
hibpdf <- alldfbp[row.names(alldfbp) %in% row.names(godepthdf[godepthdf$depth > 12,,drop=F]),]
#alllpsbp <- alldfbp[(row.names(alldfbp) %like% "lipopolysaccharide") | (row.names(alldfbp) %like% "lipid A"),]

#breaksList = seq(100, 1000, by = 50)
pheatmap(lowbpdf, main="All BP GOs (depth <5)", cluster_cols = F,annotation_col= resp2, annotation_colors = classcolor, breaks=breaksList, color=viridis(length(breaksList)))
pheatmap(midlowbpdf, main="All BP GOs (depth 5-10)", cluster_col = F, annotation_col = resp2, annotation_colors = classcolor, breaks=breaksList, color=viridis(length(breaksList)))
pheatmap(midhibpdf, main="All BP GOs (depth 11-12)", cluster_cols = F, annotation_col = resp2, annotation_colors = classcolor, breaks=breaksList, color=viridis(length(breaksList)))
pheatmap(hibpdf, main="All BP GOs (depth > 12)", cluster_cols = F, annotation_col = resp2, annotation_colors = classcolor, breaks=breaksList, color=viridis(length(breaksList)))
```

```{r eval=FALSE, fig.height=20, fig.width=20, include=FALSE}
pheatmap(alllpsbp, main="BP_LPS", cluster_cols = F, annotation_col = resp2, annotation_colors = classcolor, fontsize = 20, color=viridis((10)))
```

```{r fig.height=50, fig.width=36}
#breaksList = seq(100, 1000, by = 50)
lowbpdf <- alldfbpfilt[row.names(alldfbpfilt) %in% row.names(godepthdf[godepthdf$depth < 5 ,,drop=F]),]
midlowbpdf <- alldfbpfilt[row.names(alldfbpfilt) %in% row.names(godepthdf[(godepthdf$depth > 4) & (godepthdf < 11) ,,drop=F]),]
midhibpdf <- alldfbpfilt[row.names(alldfbpfilt) %in% row.names(godepthdf[(godepthdf$depth > 10) & (godepthdf < 13) ,,drop=F]),]
hibpdf <- alldfbpfilt[row.names(alldfbpfilt) %in% row.names(godepthdf[godepthdf$depth > 12,,drop=F]),]

pheatmap(lowbpdf, main="All BP GOs (depth <5)", cluster_cols = F,annotation_col= resp2, annotation_colors = classcolor, color=viridis(20), fontsize = 30)
pheatmap(midlowbpdf, main="All BP GOs (depth 5-10)", cluster_col = F, annotation_col = resp2, annotation_colors = classcolor, color=viridis(20), fontsize = 20)
pheatmap(midhibpdf, main="All BP GOs (depth 11-12)", cluster_cols = F, annotation_col = resp2, annotation_colors = classcolor, color=viridis(20), fontsize = 20)
##no hibpdf
#pheatmap(hibpdf, main="All BP GOs (depth > 12)", cluster_cols = F, annotation_col = resp2, annotation_colors = classcolor, color=viridis(20), fontsize = 16)
```

```{r fig.height=50, fig.width=36}
#breaksList = seq(100, 1000, by = 50)
lowmfdf <- alldfmffilt[row.names(alldfmffilt) %in% row.names(godepthdf[godepthdf$depth < 5 ,,drop=F]),]
midlowmfdf <- alldfmffilt[row.names(alldfmffilt) %in% row.names(godepthdf[(godepthdf$depth > 4) & (godepthdf < 11) ,,drop=F]),]
midhimfdf <- alldfmffilt[row.names(alldfmffilt) %in% row.names(godepthdf[(godepthdf$depth > 10) & (godepthdf < 13) ,,drop=F]),]
himfdf <- alldfmffilt[row.names(alldfmffilt) %in% row.names(godepthdf[godepthdf$depth > 12,,drop=F]),]

pheatmap(lowmfdf, main="All MF GOs (depth <5)", cluster_cols = F,annotation_col= resp2, annotation_colors = classcolor, color=viridis(20), fontsize = 30)
pheatmap(midlowmfdf, main="All MF GOs (depth 5-10)", cluster_col = F, annotation_col = resp2, annotation_colors = classcolor, color=viridis(20), fontsize = 20)
##no midhimfdf
#pheatmap(midhimfdf, main="All MF GOs (depth 11-12)", cluster_cols = F, annotation_col = resp2, annotation_colors = classcolor, color=viridis(20), fontsize = 20)
##no himfdf
#pheatmap(himfdf, main="All MF GOs (depth > 12)", cluster_cols = F, annotation_col = resp2, annotation_colors = classcolor, color=viridis(20), fontsize = 30)
```

```{r}
sessionInfo()
```